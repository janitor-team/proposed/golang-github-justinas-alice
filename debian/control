Source: golang-github-justinas-alice
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Raúl Benencia <rul@kalgan.cc>,
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any,
Standards-Version: 4.2.1
Homepage: https://github.com/justinas/alice
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-justinas-alice
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-justinas-alice.git
XS-Go-Import-Path: github.com/justinas/alice
Testsuite: autopkgtest-pkg-go

Package: golang-github-justinas-alice-dev
Architecture: all
Depends: ${misc:Depends},
Description: Painless middleware chaining for Go
 Alice provides a convenient way to chain HTTP middleware functions and
 the app handler.
 .
 It transforms:
   go Middleware1(Middleware2(Middleware3(App)))
 to
   go alice.New(Middleware1, Middleware2, Middleware3).Then(App)
 .
 None of the other middleware chaining solutions behaves exactly
 like Alice. Alice is as minimal as it gets: in essence, it's just a
 for loop that does the wrapping for you.
